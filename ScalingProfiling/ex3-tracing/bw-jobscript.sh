#!/bin/bash -l
#PBS -l nodes=1:ppn=32:xe
#PBS -l walltime=15:00
#PBS -j oe
#PBS -N ex3

# In this exercise, we will again run our TAU-instrumented miniFE, but with
# tracing enabled

# This script assumes that the training tar file is unpacked into 
#   $HOME/$training_dir
# and that the jobs will be run in
#   $HOME/scratch/$training_dir
# If you unpacked the tar file to somewhere else, change this line accordingly:
training_dir=${TUT_PATH:-s2pi2017/ScalingProfiling}
SCRATCH=$HOME/scratch
. /opt/modules/default/init/bash
module swap PrgEnv-cray PrgEnv-intel
module load tau

cd $PBS_O_WORKDIR

# path to the miniFE executable we built:
# we'll re-use the instrumented executable from ex2
ex=$HOME/$training_dir/ex2-profiling/build/miniFE-tau.x

# A 150x150x150 miniFE job should finish within 5 minutes on a single core 
# of a BW node
sz=150
cmd="$ex -nx $sz -ny $sz -nz $sz"

# Each BlueWaters core has 2 hyperthreads, which PBS views as 2 CPUs, but 
# for this exercise we want each MPI task to have its own core. The following
# recipe calculates how many MPI ranks we can run on a given number of nodes:
max_tasks_per_core=1
hyperthreads_per_core=$(lscpu | awk '/^Thread\(s\) per core/ {print $NF}')
cpus_per_node=$(lscpu | awk '/^CPU\(s\):/ {print $NF}')
cpus_per_task=$(( hyperthreads_per_core/max_tasks_per_core ))
max_mpi_per_node=$((cpus_per_node/cpus_per_task))
max_mpi_ranks=$((PBS_NUM_NODES * max_mpi_per_node))

# we will still time the overall run, to get a sense of how much overhead 
# profiling added. We'll output the time in seconds to be consistent with ex1
TIMEFORMAT=%R

# a report of communication between each pair of ranks is interesting:
export TAU_COMM_MATRIX=1

# this time we'll enable tracing:
export TAU_TRACE=1

# we'll do only a single trace run for now, using 8 ranks:
nranks=8
label=tau_trace-sz${sz}-${PBS_NUM_NODES}n-${nranks}mpi-$PBS_JOBID
rundir=$SCRATCH/$training_dir/ex3-tracing/$label
mkdir -p $rundir
cd $rundir

echo "tracing miniFE with $nranks MPI processes on $SLURM_NNODES nodes at `date`"
time aprun -n $nranks $cmd > stdout 2> stderr   

# the TAU trace output needs to be converted to a format that the trace 
# visualizer can use. Jumpshot, Vampir and Paraver can all display a trace, 
# we'll use jumpshot because it comes with Tau.
# Jumpshot reads traces in slog2 format, so we'll need to first merge the 
# traces for each process and then convert the result to slog2.
tau_treemerge.pl || { echo "treemerge step failed!" ; exit ; }
# the default java settings for tau2slog dont allow enough memory, so we'll 
# make a modified launcher:
sed -e 's/Xmx500m/Xmx1500m/' `which tau2slog2` > tau2slog2
chmod +x tau2slog2
# tau2slog produces an enormous number of warnings, so redirect them to a file
./tau2slog2 tau.trc tau.edf -o tau-${nranks}p.slog2 > tau2slog-msgs.txt 2>&1

echo "finished tracing miniFE at `date`"
