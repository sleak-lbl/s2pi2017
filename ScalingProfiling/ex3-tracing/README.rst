Scaling to Petascale Institute - Scaling and Profiling Workshop
===============================================================

Exercise 3: Tracing miniFE with TAU
-----------------------------------

In this exercise we will run our TAU-instrumented miniFE executable with 
``TAU_TRACING`` enabled, then explore its behavior using Jumpshot 

.. _Step 1:

Step 1: Run the tracing version of the job
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Take a look at ``cori-jobscript.sh``, or ``bw-jobscript.sh``. This time we're 
only doing a single run, with 8 MPI ranks, but we've set ``TAU_TRACE=1``. At 
the end of the run, we convert the TAU tracing output to slog2, the format 
used by Jumpshot (a visualizer included in the TAU suite)::

  $ sbatch cori-jobscript.sh

Or::

  $ qsub bw-jobscript.sh


.. _Step 2:

Step 2: Check that the trace was successfully created and converted
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Tracing is usually a very-high-overhead operation, regardless of which tool you
use, and trace files are typically enormous. If your tool supports pausing and 
resuming tracing (usually via API calls), then using that feature to limit a 
trace to the small region you need to invistgate is **highly** recommended.

In this exercise we traced an entire (short) run on 8 ranks, and the resulting 
trace is about 1GB in size. If everything worked you should see a set of files 
in your run directory like::

  $ lt -h
  total 2.5G
  -rw-rw---- 1 sleak sleak 635M Jun 25 00:13 tau-8p.slog2
  -rw-rw---- 1 sleak sleak  16M Jun 25 00:13 tau2slog-msgs.txt
  -rw-r----- 1 sleak sleak 935M Jun 25 00:12 tau.trc
  -rw-rw---- 1 sleak sleak  20K Jun 25 00:12 tau.edf
  -rw-rw---- 1 sleak sleak  19K Jun 25 00:12 events.0.edf
  -rw-rw---- 1 sleak sleak  17K Jun 25 00:12 events.1.edf
  -rw-rw---- 1 sleak sleak  17K Jun 25 00:12 events.2.edf
  -rw-rw---- 1 sleak sleak  17K Jun 25 00:12 events.3.edf
  -rw-rw---- 1 sleak sleak  17K Jun 25 00:12 events.4.edf
  -rw-rw---- 1 sleak sleak  17K Jun 25 00:12 events.5.edf
  -rw-rw---- 1 sleak sleak  17K Jun 25 00:12 events.6.edf
  -rw-rw---- 1 sleak sleak  17K Jun 25 00:12 events.7.edf
  -rw-rw---- 1 sleak sleak 1.7K Jun 25 00:12 miniFE.200x200x200.P8.2017:06:25-00:12:26.yaml
  -rw-rw---- 1 sleak sleak  832 Jun 25 00:12 stdout
  -rw-rw---- 1 sleak sleak 117M Jun 25 00:12 tautrace.0.0.0.trc
  -rw-rw---- 1 sleak sleak 117M Jun 25 00:12 tautrace.1.0.0.trc
  -rw-rw---- 1 sleak sleak 117M Jun 25 00:12 tautrace.2.0.0.trc
  -rw-rw---- 1 sleak sleak 117M Jun 25 00:12 tautrace.3.0.0.trc
  -rw-rw---- 1 sleak sleak 117M Jun 25 00:12 tautrace.4.0.0.trc
  -rw-rw---- 1 sleak sleak 117M Jun 25 00:12 tautrace.5.0.0.trc
  -rw-rw---- 1 sleak sleak 117M Jun 25 00:12 tautrace.6.0.0.trc
  -rw-rw---- 1 sleak sleak 117M Jun 25 00:12 tautrace.7.0.0.trc
  -rw-rw---- 1 sleak sleak    0 Jun 25 00:11 stderr

And the last few lines of ``tau2slog-msgs.txt`` will look like::

  $ tail tau2slog-msgs.txt
  TreeDir     is FBinfo(648452 @ 664815925)
  Annotations is FBinfo(0 @ 0)
  Postamble   is FBinfo(0 @ 0)

  1521 enters: 0 exits: 0

  Number of Drawables = 18985957
  timeElapsed between 1 & 2 = 135 msec
  timeElapsed between 2 & 3 = 32851 msec

If you don't see this, the most likely causes are either:

- the job ran out of time and was killed before the trace could be completed.
  If this is the case, the ``tau.trc`` and ``tau-8p.slog2`` will likely be 
  missing. If the run itself completed but converting the trace did not then
  you might be able to re-run the ``tau_treemerge.pl`` and ``tau2slog2`` commands
  commands from the end of ``cori-jobscript`` (or ``bw-jobscript``).
- the ``tau2slog2`` utility ran out of memory (due to the large trace size)
  ``tau2slog2`` is a script which calls a java program and may set the virtual 
  memory limit too low. If ``tau2slog2`` crashes, try making a local copy of the 
  script and increasing the ``-Xmx`` option passed to ``java``:: 

    $ cp `which tau2slog2` .
    $ tail -5 tau2slog2
    #sleak: 500m is not enough, edit your copy to make it 1500m:
    #java -Xmx500m -Xms32m -cp ${TAU_CLASSPATH} ${MAIN_CLASS} $@
    java -Xmx1500m -Xms32m -cp ${TAU_CLASSPATH} ${MAIN_CLASS} $@
    
You can then use ./tau2slog2 to manually retry the ``tau2slog2`` step of 
``cori-jobscript.sh``

**Note** that on Cori, ``tau2slog2`` in the tau module has this patch already
applied, and the ``bw-jobscript.sh`` has an addition to apply the patch 
in the job


.. _Step 3:

Step 3: Explore the trace with Jumpshot
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

On Cori it is **highly recommended** to use NX_ for this, as Jumpshot is very 
GUI-heavy. 

.. _NX: http://www.nersc.gov/users/connecting-to-nersc/using-nx/

Start jumpshot in the run directory with::

  $ cd /path/to/run/dir
  $ module load tau
  $ jumpshot tau-8p.slog2

On Blue Waters you may need to apply the same patch for the java command to 
jumpshot - if you experience jumpshot crashing, try the following::

  $ jumpshot=/sw/xe/tau/2.25.1/cle5.2_intel16.0.3/craycnl/bin/jumpshot
  $ sed -e 's/Xmx500m/Xmx1500m/' $jumpshot > ./jumpshot
  $ chmod +x ./jumpshot

The jumpshot interface is complex, we'll step through a few facets of it in 
the slides.
