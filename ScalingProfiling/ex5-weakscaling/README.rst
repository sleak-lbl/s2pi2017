Scaling to Petascale Institute - Scaling and Profiling Workshop
===============================================================

Exercise 5: Weak scaling
------------------------

In this exercise we run the miniFE executable we built in `exercise 1`_, but 
we'll try to scale it to a higher node count. Since we know the performance
of our size=200 run drops off around 1 node, we'll increase the problem size
for higher core counts, and plot the scaling at a few problem sizes.

Because the problem sizes are larger, the jobs in this exercise will run for
longer than previous exercises. **To be polite** we'll break the runs into a 
series of jobs, putting all of the single node runs together, then some 2-node
runs, then some 4-node runs. This session starts after a short break, so 
submitting at least some of the jobs before the break is recommended.

.. _Step 1:

Step 1: Run a few different job sizes at a few different scales
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Submit ``cori-jobscript.sh`` with ``sbatch`` - or ``bw-jobscript.sh`` with 
``qsub``, then take a look at the script. There are a few ``#SBATCH`` (or 
``#PBS``) directives for number of nodes and corresponding walltime, the 
first one is for a single node. Comment it out and uncomment a different 
nodecount, then resubmit the job.

This job script has 2 loops: first it loops over some problem sizes, then
for each problem size does a series of runs with different core counts. The 
range of core counts is adjusted with the problem size, so larger problems 
run on more cores. 

This time as well as timing we'll collect the flop count as reported by
miniFE, and also it's calculation of the speed (MFLOPS) of the main solver,
and record them in a ``timings.dat`` file that we can then plot with gnuplot.
We'll use a different ``timings.dat`` file for each problem size.


.. _Step 2:

Step 2: Plot the scaling for different job sizes
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

You should now have a set of files like ``sz100-timings.dat``, that look
something like::

  $ cat sz100-timings.dat
  #nranks      flopcnt      walltime      bm_time      CG_MFlops
  64      1.35689e+10      5.593      0.580871      30541.3
  128      1.35689e+10      5.435      0.277371      66283.2
  2      1.35689e+10      12.739      6.68554      3175.22
  4      1.35689e+10      9.196      3.56501      6109.22
  8      1.35689e+10      10.175      2.14442      10852.5
  16      1.35689e+10      4.033      1.42527      14636.6
  32      1.35689e+10      5.798      1.14195      15386.9

Notice that the rows are not in order, this is because we used multiple jobs to
collect the timings. We could sort them, but we'll use gnuplot's 
``smooth unique`` (``s u``) filter to do it for us.

We have a MFlops rating as reported by miniFE, but this is only for the CG 
kernel and we're interested in the whole-run scaling. So we'll calculate GFLOPS
based on the walltime and the flop count, and plot that::

  $ gnuplot
  gnuplot> f100="sz100-timings.dat"
  gnuplot> f200="sz200-timings.dat"
  gnuplot> f300="sz300-timings.dat"
  gnuplot> f400="sz400-timings.dat"
  gnuplot> real_gf(flopcount,time) = (flopcount/1e9)/time
  gnuplot> set logscale
  gnuplot> set xlabel "nproc"
  gnuplot> set ylabel "gflops"
  gnuplot> plot f100 u 1:(real_gf($2,$3)) w lp t "sz=100" s u, \
  >             f200 u 1:(real_gf($2,$3)) w lp t "sz=200" s u, \
  >             f300 u 1:(real_gf($2,$3)) w lp t "sz=300" s u, \
  >             f400 u 1:(real_gf($2,$3)) w lp t "sz=400" s u

As an extension, try plotting the ``CG_MFlops`` - you might see a rather 
different outcome! It would appear that the performance of the CG kernels
scales better for small problems - the likely cause is that the small problem
fits better in cache, while the larger problem sizes suffer more delays due to 
cache misses. 
