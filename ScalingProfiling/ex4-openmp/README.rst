Scaling to Petascale Institute - Scaling and Profiling Workshop
===============================================================

Exercise 4: Profiling an OpenMP application for scaling issues
--------------------------------------------------------------

In this exercise we will build an OpenMP-enabled miniFE executable - with TAU
instrumentation - run it with 1, 2, 4, etc threads and look at its scaling.
It will become apparent that this version of miniFE does not use OpenMP as 
effectively as it uses MPI, so we'll use TAU to identify some of the factors 
limiting miniFE's OpenMP scalability.

.. _Step 1:

Step 1: Build the executable
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``cd`` into the ``build`` directory and take a look at the ``Makefile``. Notice
that we have explicitly enabled OpenMP.

We plan to look at the TAU profile, so make sure TAU is loaded into your 
enviroment, then build the executable::

  $ cd build
  $ module load tau
  $ make

The makefile might remind you to set certain environment variables first.
 
.. _Step 2:

Step 2: Run at a few levels of OpenMP scaling
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Take a look at ``cori-jobscript.sh`` (or ``bw-jobscript.sh``). Notice that this
time we are only using 1 MPI rank, but that we've added some OpenMP settings. 
At the end of each run it will write to a timing file, in a similar manner to 
`exercise 1`_. Submit the job::

  $ sbatch cori-runscript.sh

Or::

  $ qsub bw-jobscript.sh

.. _exercise 1: file://../ex1-scaling/README.rst


.. _Step 3:

Step 3: Plot the timings against OMP_NUM_THREADS
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

At the end of the job, you should see a file like ``timings-5381580.dat`` (In
fact, the file will grow during the run, so you can plot the first few points
before it begins, if you like)::

  $ cat timings-5381580.dat
  #nthreads      bm_time      walltime
    1      132.885      135.100
    2      119.234      124.298
    4      119.712      120.618
    16     143.711      146.979
    32     197.472      206.260

A cursory look at the timings is enough to see that miniFE is not scaling well
with OpenMP - but we can plot it with gnuplot anyway. We'll just plot time,
but using a log scale::

  $ gnuplot
  gnuplot> f="timings-5381580.dat"
  gnuplot> set xlabel "OMP_NUM_THREADS"
  gnuplot> set ylabel "sec"
  gnuplot> set logscale
  gnuplot> plot f u 1:2 w lp t "BM time", f u 1:3 w lp t "Walltime"


.. _Step 4:

Step 4: Use TAU to identify some scaling limiters
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Select one or two runs to look at more closely, and use TAU paraprof to explore
and compare the profiles (you can open 2 profiles together by first packing 
each into a single file, with ``paraprof --pack pack_file_name.ppk``)::

  $ module load tau
  $ # on Blue Waters, change $SCRATCH to $HOME/scratch
  $ cd $SCRATCH/path/to/ex4/tau_profile-sz200-1n-2omp-5381580
  $ paraprof --pack omp2.ppk
  $ ls -lt
  -rw-rw---- 1 sleak sleak  8880 Jun 24 21:05 omp2.ppk
  -rw-rw---- 1 sleak sleak  1665 Jun 24 20:01 miniFE.200x200x200.P1.2017:06:24-20:01:59.yaml
  $ cd ../tau_profile-sz200-1n-16omp-5381580
  $ paraprof --pack omp16.ppk
  $ paraprof */*.ppk

We'll explore the results in the slides.


.. _Extension:

Extension: Look at routine scaling 
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

As an optional extra, we can re-use the script from `exercise 2`_ to look at 
routine scaling::

  $ module load tau
  $ run1p=/path/to/1p/run  # eg $SCRATCH/path/to/ex4/tau_profile-sz200-1n-1omp-5381580/
  $ run2p=/path/to/2p/run  
  ...
  $ run16p=/path/to/16p/run  
  $ run32p=/path/to/32p/run  
  $ pp=/path/to/parse_pprofs.py   # eg $HOME/s2pi2017/ex2-profiling/parse_pprofs.py
  $ python $pp -t 6 -n 1 -d $run1p -n 2 -d $run2p -n 4 -d $run4p -n 8 -d $run8p -n 16 -d $run16p -n 32 -d $run32p > routine_scaling.dat

Note that the parse_pprofs.py script will try to extract a short name for
each routine, which might require some interpreting.

.. _exercise 2: file://../ex2-profiling/README.rst

Which we can again plot with gnuplot::

  $ gnuplot
  gnuplot> f="routine_scaling.dat"
  gnuplot> nroutines=6
  gnuplot> set key autotitle columnheader
  gnuplot> set key outside top right
  gnuplot> set xlabel "nproc"
  gnuplot> set ylabel "msec"
  gnuplot> set logscale
  gnuplot> plot for [i=2:nroutines+1] f u 1:i w lp


